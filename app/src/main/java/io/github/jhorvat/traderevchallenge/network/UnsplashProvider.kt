package io.github.jhorvat.traderevchallenge.network

import android.arch.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object UnsplashProvider {
    val photos = MutableLiveData<List<Photo>>().apply { value = mutableListOf() }
    val loading = MutableLiveData<Boolean>().apply { value = false }

    private var nextPage = 1
    private val unsplashService = Retrofit.Builder()
            .baseUrl("https://api.unsplash.com")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(UnsplashService::class.java)

    fun fetchPhotos() {
        loading.value = true
        unsplashService.photos(nextPage)
                .enqueue(object : Callback<List<Photo>> {
                    override fun onResponse(call: Call<List<Photo>>?, response: Response<List<Photo>>?) {
                        loading.value = false
                        response?.body()
                                .takeIf { it?.isNotEmpty() ?: false }
                                ?.let { newPhotos ->
                                    photos.value?.plus(newPhotos)?.let {
                                        photos.postValue(it)
                                        nextPage++
                                    }
                                }
                    }

                    override fun onFailure(call: Call<List<Photo>>?, t: Throwable?) {
                        loading.value = false
                    }
                })
    }
}