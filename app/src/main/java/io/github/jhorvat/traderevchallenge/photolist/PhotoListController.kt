package io.github.jhorvat.traderevchallenge.photolist

import com.airbnb.epoxy.Typed2EpoxyController
import io.github.jhorvat.traderevchallenge.network.Photo

class PhotoListController(
        private inline val onPhotoClicked: (Int) -> Unit
) : Typed2EpoxyController<List<Photo>, Boolean>() {

    override fun buildModels(photos: List<Photo>?, loading: Boolean?) {
        photos?.forEachIndexed { i, photo ->
            val photoModel = PhotoListItemBinder(photo) {
                onPhotoClicked(i)
            }

            add(photoModel)
        }

        if (loading != true) return

        add(LoadingBinder())
    }
}