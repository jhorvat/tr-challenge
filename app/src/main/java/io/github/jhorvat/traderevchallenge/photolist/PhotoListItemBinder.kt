package io.github.jhorvat.traderevchallenge.photolist

import android.databinding.ViewDataBinding
import com.airbnb.epoxy.DataBindingEpoxyModel
import com.squareup.picasso.Picasso
import io.github.jhorvat.traderevchallenge.R
import io.github.jhorvat.traderevchallenge.databinding.ItemPhotoThumbnailBinding
import io.github.jhorvat.traderevchallenge.network.Photo

class PhotoListItemBinder(
        private val photo: Photo,
        private inline val onClicked: () -> Unit
) : DataBindingEpoxyModel() {

    init {
        id(photo.hashCode())
    }

    override fun setDataBindingVariables(binding: ViewDataBinding?) {
        (binding as? ItemPhotoThumbnailBinding)?.run {
            Picasso.with(root.context)
                    .load(photo.urls.small)
                    .into(img)

            root.setOnClickListener {
                onClicked()
            }
        }
    }

    override fun unbind(holder: DataBindingHolder) {
        super.unbind(holder)
        (holder.dataBinding as? ItemPhotoThumbnailBinding)?.run {
            Picasso.with(root.context).cancelRequest(img)
        }
    }

    override fun getDefaultLayout() = R.layout.item_photo_thumbnail
}