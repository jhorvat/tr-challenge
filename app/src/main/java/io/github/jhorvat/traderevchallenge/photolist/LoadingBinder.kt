package io.github.jhorvat.traderevchallenge.photolist

import android.databinding.ViewDataBinding
import com.airbnb.epoxy.DataBindingEpoxyModel
import io.github.jhorvat.traderevchallenge.R

class LoadingBinder: DataBindingEpoxyModel() {

    init {
        id(R.layout.item_loading)
    }

    override fun setDataBindingVariables(binding: ViewDataBinding?) {
    }

    override fun getDefaultLayout() = R.layout.item_loading
}