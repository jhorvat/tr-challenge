package io.github.jhorvat.traderevchallenge.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

private const val UNSPLASH_KEY = "cac5ae6f7cd746d3ddb828e69667e07f4b40f8a6f93a2674081fcadb4dd2e2f9"
private const val PAGE_LIMIT = 25

interface UnsplashService {
    @GET("/photos?per_page=$PAGE_LIMIT&client_id=$UNSPLASH_KEY")
    fun photos(@Query("page") page: Int): Call<List<Photo>>
}