package io.github.jhorvat.traderevchallenge.photopager

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import io.github.jhorvat.traderevchallenge.R
import io.github.jhorvat.traderevchallenge.network.UnsplashProvider

class PhotoPagerAdapter : PagerAdapter() {

    private val pageBinder = PhotoPageBinder()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val photo = UnsplashProvider.photos.value?.get(position)
                ?: return super.instantiateItem(container, position)

        return pageBinder.createAndBindPhotoPage(container, photo).also {
            container.addView(it)
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        (obj as? View)?.let { root ->
            Picasso.with(container.context)
                    .cancelRequest(root.findViewById<ImageView>(R.id.img))
            container.removeView(root)
        }
    }

    override fun isViewFromObject(view: View, obj: Any) = view == obj

    override fun getCount() = UnsplashProvider.photos.value?.size ?: 0
}