package io.github.jhorvat.traderevchallenge.extensions

import android.support.v7.widget.RecyclerView

inline fun RecyclerView.addOnScrollListener(crossinline lambda: (rv: RecyclerView?, dx: Int, dy: Int) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            lambda(recyclerView, dx, dy)
        }
    })
}