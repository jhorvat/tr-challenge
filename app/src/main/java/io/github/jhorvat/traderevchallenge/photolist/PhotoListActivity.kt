package io.github.jhorvat.traderevchallenge.photolist

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import io.github.jhorvat.traderevchallenge.R
import io.github.jhorvat.traderevchallenge.extensions.addOnScrollListener
import io.github.jhorvat.traderevchallenge.extensions.observe
import io.github.jhorvat.traderevchallenge.network.UnsplashProvider
import io.github.jhorvat.traderevchallenge.photopager.PhotoPagerActivity


class PhotoListActivity : AppCompatActivity() {

    companion object {
        private const val ARG_LAST_POSITION = "LAST_POSITION"
        private const val REQUEST_CODE_PHOTO_PAGER = 1

        fun getReturnIntent(lastPosition: Int) = Intent().apply {
            putExtra(ARG_LAST_POSITION, lastPosition)
        }
    }

    private val photoListController: PhotoListController by lazy {
        PhotoListController { photoIndexToStartOn ->
            startActivityForResult(
                    PhotoPagerActivity.getIntent(this, photoIndexToStartOn),
                    REQUEST_CODE_PHOTO_PAGER
            )
        }
    }

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_list)
        recyclerView = findViewById<RecyclerView>(R.id.recycler).apply {
            bindPhotoList()
        }

        UnsplashProvider.photos.observe(this) {
            photoListController.setData(it, UnsplashProvider.loading.value)
        }

        UnsplashProvider.loading.observe(this) {
            photoListController.setData(UnsplashProvider.photos.value, it)
        }

        UnsplashProvider.fetchPhotos()
    }

    private fun RecyclerView.bindPhotoList() {
        val gridManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)

        layoutManager = gridManager
        adapter = photoListController.adapter

        addOnScrollListener { _, _, _ ->
            // Don't bother if we're already loading
            if (UnsplashProvider.loading.value == true) return@addOnScrollListener

            val lastVisiblePosition = gridManager.findLastVisibleItemPositions(null).max()
            if (gridManager.itemCount - 1 == lastVisiblePosition) {
                UnsplashProvider.fetchPhotos()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_PHOTO_PAGER && resultCode == RESULT_OK) {
            recyclerView.post {
                val lastPosition = data?.getIntExtra(ARG_LAST_POSITION, 0) ?: 0
                recyclerView.scrollToPosition(lastPosition)
            }
        }
    }
}
