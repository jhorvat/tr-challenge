package io.github.jhorvat.traderevchallenge.photopager

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import io.github.jhorvat.traderevchallenge.R
import io.github.jhorvat.traderevchallenge.extensions.observe
import io.github.jhorvat.traderevchallenge.network.UnsplashProvider
import io.github.jhorvat.traderevchallenge.photolist.PhotoListActivity


class PhotoPagerActivity : AppCompatActivity() {

    companion object {
        private const val ARG_STARTING_INDEX = "STARTING_INDEX"

        fun getIntent(context: Context, startingIndex: Int) =
                Intent(context, PhotoPagerActivity::class.java).apply {
                    putExtra(ARG_STARTING_INDEX, startingIndex)
                }
    }

    private val pagerAdapter: PhotoPagerAdapter by lazy(::PhotoPagerAdapter)
    private lateinit var pager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE

        setContentView(R.layout.activity_photo_pager)
        pager = findViewById<ViewPager>(R.id.pager).apply {
            bindPhotoPager()
        }

        UnsplashProvider.photos.observe(this) {
            pagerAdapter.notifyDataSetChanged()
        }
    }

    private fun ViewPager.bindPhotoPager() {
        adapter = pagerAdapter
        offscreenPageLimit = 4
        currentItem = intent.getIntExtra(ARG_STARTING_INDEX, 0)

        addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                // Don't fire again if we're already loading
                if (UnsplashProvider.loading.value == true) return

                val photoListSize = UnsplashProvider.photos.value?.size ?: 0
                // If we're within 4 photos of the end start a fetch
                if (position in (photoListSize - 3..photoListSize)) {
                    UnsplashProvider.fetchPhotos()
                }
            }
        })
    }

    override fun finish() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        setResult(RESULT_OK, PhotoListActivity.getReturnIntent(pager.currentItem))
        super.finish()
    }
}