package io.github.jhorvat.traderevchallenge.photopager

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import io.github.jhorvat.traderevchallenge.R
import io.github.jhorvat.traderevchallenge.databinding.ItemPhotoPageBinding
import io.github.jhorvat.traderevchallenge.network.Photo

class PhotoPageBinder {

    fun createAndBindPhotoPage(container: ViewGroup, photo: Photo): View {
        val context = container.context

        val binding = ItemPhotoPageBinding.inflate(LayoutInflater.from(context), container, false).apply {
            txtUserName.text = context.getString(R.string.img_posted_by, photo.user.username)
            txtLikes.text = context.resources.getQuantityString(R.plurals.img_likes, photo.likes, photo.likes)
            bindDescription(photo)
            bindPhoto(context, photo)
        }

        return binding.root
    }

    private fun ItemPhotoPageBinding.bindDescription(photo: Photo) {
        if (photo.description.isNullOrBlank()) return

        txtDescription.visibility = View.VISIBLE
        txtDescription.text = photo.description
    }

    private fun ItemPhotoPageBinding.bindPhoto(context: Context, photo: Photo) {
        Picasso.with(context)
                .load(photo.urls.regular)
                .into(img)
    }
}