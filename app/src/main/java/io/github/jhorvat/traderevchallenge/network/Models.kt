package io.github.jhorvat.traderevchallenge.network

data class Photo(
        val id: String,
        val likes: Int,
        val description: String?,
        val user: User,
        val urls: Urls
)

data class Urls(val regular: String, val small: String)

data class User(val username: String)
